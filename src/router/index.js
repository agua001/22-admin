// 引入到main.js
import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login.vue'
// 引入首页组件
import Home from '@/views/Home.vue'
import Welcome from '@/views/Welcome'
// 用户列表页面
import Users from '@/views/Users'
// 引入权限列表页面
import Rights from '@/views/auth/Rights'
import Roles from '@/views/auth/Roles'
import Categories from '@/views/goods/Categories'
import Goods from '@/views/goods/Goods'
import AddGoods from '@/views/goods/AddGoods'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: { name: 'welcome' },
      children: [
        {
          name: 'welcome',
          path: 'welcome',
          component: Welcome
        },
        {
          name: 'users',
          path: 'users',
          component: Users
        },
        {
          name: 'rights',
          path: 'rights',
          component: Rights
        },
        {
          name: 'roles',
          path: 'roles',
          component: Roles
        },
        {
          name: 'categories',
          path: 'categories',
          component: Categories
        },
        {
          name: 'goods',
          path: 'goods',
          component: Goods
        },
        {
          name: 'addgoods',
          path: 'addgoods',
          component: AddGoods
        }
      ]
    }
  ]
})
