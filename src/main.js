// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
// 引入路由
import router from './router'
// 引入element-ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 引入样式
import '@/styles/index.scss'
// 5.1 添加全局导航守卫（路由拦截）
router.beforeEach((to, from, next) => {
  // a 如果用户登录了，想要跳转到非登录页,我们让它继续，不拦它,登录(/login) 也不拦它
  // b 如果用户没有登录，想要跳转到非登录,我们要拦它，不让它跳转到首页，让它回到登录页面，登录到(/login)不拦它，让它继续
  let mytoken = localStorage.getItem('mytoken') || ''
  if (mytoken) {
    // 这里的条件是用户登录了，我们不拦它，让它一路下一步
    next()
  } else {
    // to.name必须和router/index.js中的路由规则的name对应
    if (to.name !== 'login') {
      // 未登录用户访问非登录页面，需要拦截下来，让它去到登录页
      next('/login')
    } else {
      // 未登录用户访问登录页面就直接让它访问
      next()
    }
  }
})
// 使用element-ui
Vue.use(ElementUI)
// productionTip 2.2.0 新增 类型: boolean 默认值: true 用法: 设置为 false 以阻止 vue 在启动时生成生产提示
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
